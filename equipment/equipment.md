## Widefield Microscopy
- Microscope 1 (upright) E1035CBI tel: 5239 
microscope1_151027.pdf
- Microscope 2 (upright) E1035CBI tel: 5239
microscope2_151027.pdf
- Microscope 3 (upright) E1035CBI tel: 5239
microscope3_151027.pdf
- Microscope 4 (upright) E1035CBI tel: 5239
microscope4_151027.pdf


## Macroscopy
- Epifluorescent macroscope E1042CBI (front right) tel: 5129
MacroFluo_151203.pdf


## Videomicroscopy
- Time-lapse 2 (inverted) E1037CBI (rear) tel: 5237
TimeLapse2_151203.pdf


## Laser Microdissection
- Microdissection system inverted E1042CBI (front left) tel: 5129


## Confocal Microscopy
- Leica SP5 (inverted) E1038CBI (left) tel: 5127
SP5_Inverted.pdf
- Leica SP8-MP (upright) E1003CBI tel: 5125
  ConfocalSP8MP_151203.pdf
- Leica SP8-UV (inverted) E1002CBI (rear) tel: 5122
  ConfocalSP8UV_151203.pdf
- Leica SP8-X (Inverted) E1002CBI (front) tel: 5122 
  ConfocalSP8X160201.pdf
- Yokogawa-W1 Spinning disk on Leica inverted stand E1037 CBI tel 5238
Spinning disk LEICA CSU W1.pdf
- Yokogawa-X1 spinning disk on Nikon inverted stand E1037CBI tel: 5238
Spinning_disk _Nikon.pdf


## Light Sheet Microscopy
- Zeiss LS7 E1041CBI tel: 5128
LightSheetSP8DLS_151203.pdf


## Laboratory space
- Sample preparation room:  incubators, aspiration hood, bench, binocular E1008CBI tel: 5124
- Cell culture room: incubators, bench, laminar flow hood E1033CBI tel: 5125
