## Overview of the Imaging Centre of the IGBMC
The Imaging Center of the IGBMC (ICI) offers access to advanced multiscale bio-imaging to the scientific community. The Centre is open to both academic research groups and industry clients. The Centre provides its expertise in photonic, electron microscopy, sample preparation and image analysis.
